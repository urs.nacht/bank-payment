# Flectra Community / bank-payment

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_payment_order](account_payment_order/) | 1.0.1.3.1| Account Payment Order
[account_payment_partner](account_payment_partner/) | 1.0.1.3.0| Adds payment mode on partners and invoices
[account_payment_mode](account_payment_mode/) | 1.0.1.0.1| Account Payment Mode
[account_payment_order_return](account_payment_order_return/) | 1.0.1.0.0| Account Payment Order Return
[account_banking_sepa_direct_debit](account_banking_sepa_direct_debit/) | 1.0.1.0.2| Create SEPA files for Direct Debit
[account_banking_sepa_credit_transfer](account_banking_sepa_credit_transfer/) | 1.0.1.0.0| Create SEPA XML files for Credit Transfers
[account_banking_pain_base](account_banking_pain_base/) | 1.0.1.0.1| Base module for PAIN file generation
[account_payment_sale](account_payment_sale/) | 1.0.1.0.1| Adds payment mode on sale orders
[account_banking_mandate](account_banking_mandate/) | 1.0.2.0.0| Banking mandates
[account_payment_purchase](account_payment_purchase/) | 1.0.1.0.0| Adds Bank Account and Payment Mode on Purchase Orders


